RIOT API (WordPress Plugin)
===============

This plugin is for fetching user stats from RIOT server and display on Buddypress profile
page. There is also shortcode i.e. [RIOT] for showing registration and login form 
on any page or post.
