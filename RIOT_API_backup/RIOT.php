<?php
/*
 * Plugin Name: User Stats RIOT API
 * Version: 0.1
 * Author: Arjun
*/
error_reporting(0);
if(!class_exists('RIOT')){
    class RIOT{
        private $url = 'https://{region}.api.pvp.net/api/lol/{region}/';
        private $api_key = '13fdd5c0-fb08-45db-895a-d5c6530cea1c';
        //private $summoner_name = 'RiotSchmick';
        //private $summoner_id = 585897;
        private $region;
        private $pluginPath;
	private $pluginUrl;
	private $uploadPath;
        private $wpdb;

        /**
         * Constructor
         * @param string $region
         */
        public function __construct($region) {
            // Set Plugin Path
            $this->pluginPath = dirname(__FILE__);
            // Set Plugin URL
            $this->pluginUrl = WP_PLUGIN_URL . '/RIOT_API';
            // Set Upload DIR
            $this->uploadPath = WP_UPLOAD_DIR();
            // Set WPDB Class Object
            global $wpdb;
            $this->wpdb = $wpdb;
            
            $this->region = $region;
            
            add_action('wp_enqueue_scripts', array($this, 'scripts'));
            add_shortcode('RIOT', array($this, 'front_code'));
        }
        
        /**
         * Function to add styles and scripts
         */
        public function scripts() {
            wp_register_style('custom-style', $this->pluginUrl.'/css/riot-style.css');
            wp_enqueue_style( 'custom-style' );
            
            //wp_register_script('custom-script', $this->pluginUrl.'/js/my-script.js', array('angular-script', 'jquery'));
            //wp_enqueue_script('custom-script');
        }

        /**
         * Shortcode Function Callback
         * @return type
         */
        public function front_code() {
            $error_msg = '';
            $show_register = true;
            $current_user_ID = get_current_user_id();
            
            ob_start();
            echo '<div class="riot_api">';
            
            if(isset($_POST['riot-edit-link'])){
                $gamer_id = get_user_meta($current_user_ID, 'riot_gamer_id', true);
                $region = get_user_meta($current_user_ID, 'riot_region', true);
                $this->editUser(false, '', $gamer_id, $region);
                $show_register = false;
            }
            
            if(isset($_POST['riot-edit-user'])){
                $gamer_id = sanitize_text_field($_POST['riot_id']);
                $region = strtolower(sanitize_text_field($_POST['riot-region']));
                
                update_user_meta($current_user_ID, 'riot_gamer_id', $gamer_id);
                update_user_meta($current_user_ID, 'riot_region', $region);
            }
            
            if(isset($_POST['riot-insert-user'])){
                $valid = 0;
                $fname = sanitize_text_field($_POST['riot-fname']);
                $lname = sanitize_text_field($_POST['riot-lname']);
                $email = sanitize_text_field($_POST['riot-email']);
                $username = sanitize_text_field($_POST['riot-username']);
                $password = sanitize_text_field($_POST['riot-pass']);
                $gamer_id = sanitize_text_field($_POST['riot_id']);
                $region = strtolower(sanitize_text_field($_POST['riot-region']));
                
                if(empty($fname) || empty($lname) || empty($email) || empty($username) || empty($password) || empty($gamer_id) || empty($region)){
                    $error_msg = 'All fields are mandatory!';
                }
                else{
                    $userdata = array(
                        'first_name' => $fname,
                        'last_name' => $lname,
                        'user_email' => $email,
                        'user_login' => $username,
                        'user_pass' => $password,
                        'nickname' => $username
                    );
                    $user_id = wp_insert_user($userdata);
                    
                    if(is_wp_error($user_id)) {
                        $error_msg = 'User already exists!';
                    }
                    else{
                        update_user_meta($user_id, 'riot_gamer_id', $gamer_id);
                        update_user_meta($user_id, 'riot_region', $region);
                        $valid = 1;
                    }
                }
                
                if($valid){
                    $this->login(false);
                    $show_register = false;
                }
                else{
                    $this->register(true, $error_msg);
                    $show_register = false;
                }
            }
            
            if(isset($_POST['riot-login'])){
                $creds = array();
                $creds['user_login'] = $this->wpdb->escape($_POST['riot-username']);
                $creds['user_password'] = $_POST['riot-pass'];
                //$creds['remember'] = true;
                $user = wp_signon( $creds, false );
                
                if ( is_wp_error($user) ){
                    $error_msg = $user->get_error_message();
                    $this->login(true, $error_msg);
                }else{
                    wp_redirect(get_permalink());
                }
            }
            elseif(isset($_POST['riot-register'])){
                $this->register();
            }
            elseif($show_register){
                if(!$current_user_ID){
                    $this->login();
                }
                else{
                    $id = get_user_meta($current_user_ID, 'riot_gamer_id', true);
                    $this->region = get_user_meta($current_user_ID, 'riot_region', true);
                    $this->getStatSummary($id, 'SEASON4');
                }
            }
            
            echo '</div>';
            return ob_get_clean();
        }
        
        /**
         * Function to show registration form
         * @param boolean $error
         * @param string $error_msg
         */
        public function register($error = false, $error_msg = '') {
            require_once 'includes/register.php';
        }
        
        /**
         * Function to show edit form
         * @param boolean $error
         * @param string $error_msg
         */
        public function editUser($error = false, $error_msg = '', $gamer_id, $region) {
            require_once 'includes/edit.php';
        }
        
        /**
         * Function for login form
         * @param boolean $error
         * @param string $error_msg
         */
        public function login($error = false, $error_msg = '') {
            require_once 'includes/login.php';
        }
        
        /**
         * Function for getting user stats
         * @param int $id
         * @param string $season
         */
        public function getStatSummary($id, $season) {
            echo '<div class="heading">'
                . '<h3>User Game Stats'
                . '<form method="post"><button type="submit" name="riot-edit-link" class="btn-blue">Edit</button></form>'
                . '<a class="link" href="'.wp_logout_url(get_permalink()).'" title="Logout">Logout</a>'
            . '</div>';
            
            //$url = $this->url."v1.3/stats/by-summoner/{$id}/summary?season={$season}&api_key={api_key}";
            $url = $this->url."v1.3/stats/by-summoner/{$id}/summary?api_key={api_key}";
            $info = $this->fetch($url);
            $decoded_info = json_decode($info, true);
            
            echo "<p class='id'><label>Gamer ID:</label> $id</p>";
            
            if(!empty($decoded_info)){
                foreach($decoded_info['playerStatSummaries'] as $di):
                    echo '<dl class="dl-horizontal">';

                    foreach($di as $k=>$d):

                        if(is_array($d)){
                            echo '<ul>';
                            foreach($d as $l=>$a):
                                $l = ucwords(preg_replace('/(?<!\ )[A-Z]/', ' $0', $l));
                                echo "<li>{$l}: $a</li>";
                            endforeach;
                            echo '</ul>';
                        }
                        else{
                            $k = ucwords(preg_replace('/(?<!\ )[A-Z]/', ' $0', $k));
                            echo "<dt>{$k}:</dt><dd>$d</dd>";
                        }

                    endforeach;

                    echo '</dl>';
                endforeach;
            }
            else{
                echo '<p class="error">Error fetching results! Gamer id not correct.</p>';
            }
        }

        /**
         * Function for hitting API
         * @param string $url
         * @return json
         */
        private function fetch($url) {
            // create curl resource
            $ch = curl_init();

            // set url
            $url = strtr($url, array(
                '{region}' => $this->region,
                '{api_key}' => $this->api_key
            ));
            curl_setopt($ch, CURLOPT_URL, $url);

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // $output contains the output string
            $output = curl_exec($ch);

            // close curl resource to free up system resources
            curl_close($ch);

            return $output;
        }
    }
    
    new RIOT('na');

//    $r = new RIOT('na');
//    $userStats = $r->getStatSummary(585897, 'SEASON4');
//
//    $stats = json_decode($userStats, true);
//    echo '<pre>';print_r($stats);
}
?>