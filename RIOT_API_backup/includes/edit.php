<h3>Edit User</h3>
<form method="post" class="form">
    <?php
        if($error){
            echo "<p class='error'>$error_msg</p>";
        }
    ?>
    <p>
        <input type="text" name="riot_id" value="<?php echo $gamer_id; ?>" placeholder="Riot Gamer ID"/>
    </p>
    <p>
    <select name="riot-region">
        <option <?php echo $region=="br"?'selected':''; ?> value="BR">Brazil</option>
        <option <?php echo $region=="eune"?'selected':''; ?> value="EUNE">EU Nordic &amp; East</option>
        <option <?php echo $region=="euw"?'selected':''; ?> value="EUW">EU West</option>
        <option <?php echo $region=="kr"?'selected':''; ?> value="KR">Korea</option>
        <option <?php echo $region=="lan"?'selected':''; ?> value="LAN">Latin America North</option>
        <option <?php echo $region=="las"?'selected':''; ?> value="LAS">Latin America South</option>
        <option <?php echo $region=="na"?'selected':''; ?> value="NA">North America</option>
        <option <?php echo $region=="oce"?'selected':''; ?> value="OCE">Oceania</option>
        <option <?php echo $region=="ru"?'selected':''; ?> value="RU">Russia</option>
        <option <?php echo $region=="tr"?'selected':''; ?> value="TR">Turkey</option>
    </select>
    </p>
    <p class="submit">
        <button type="submit" class="link" name="riot-red">Cancel</button>
        <button type="submit" name="riot-edit-user" class="btn-blue">Save Changes</button>
    </p>
</form>