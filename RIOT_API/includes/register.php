<h3>Register</h3>
<form method="post" class="form">
    <?php
        if($error){
            echo "<p class='error'>$error_msg</p>";
        }
    ?>
    <p>
        <input type="text" name="riot-fname" placeholder="First Name"/>
    </p>
    <p>
        <input type="text" name="riot-lname" placeholder="Last Name"/>
    </p>
    <p>
        <input type="email" name="riot-email" placeholder="Email Address"/>
    </p>
    <p>
        <input type="text" name="riot-username" placeholder="Username"/>
    </p>
    <p>
        <input type="password" name="riot-pass" placeholder="Password"/>
    </p>
    <p>
        <input type="text" name="riot_id" placeholder="Riot Gamer ID" autocomplete="off"/>
    </p>
    <p>
        <input type="text" name="riot_name" placeholder="Summoner Name (verfication)" autocomplete="off"/>
    </p>
    <p>
    <select name="riot-region">
        <option value="BR">Brazil</option>
        <option value="EUNE">EU Nordic &amp; East</option>
        <option value="EUW">EU West</option>
        <option value="KR">Korea</option>
        <option value="LAN">Latin America North</option>
        <option value="LAS">Latin America South</option>
        <option selected="selected" value="NA">North America</option>
        <option value="OCE">Oceania</option>
        <option value="RU">Russia</option>
        <option value="TR">Turkey</option>
    </select>
    </p>
    <p class="submit">
        <button type="submit" class="link" name="riot-red-login">Login</button>
        <button type="submit" name="riot-insert-user" class="btn-blue">Register</button>
    </p>
</form>