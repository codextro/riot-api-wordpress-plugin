<form method="post" class="form">
    <h3>Login</h3>
    <?php
        if($error){
            echo "<p class='error'>$error_msg</p>";
        }
    ?>
    <p>
        <input type="text" name="riot-username" placeholder="Username"/>
    </p>
    <p>
        <input type="password" name="riot-pass" placeholder="Password"/>
    </p>
    <p class="submit">
        <button type="submit" class="link" name="riot-register">Register</button>
        <button type="submit" name="riot-login" class="btn-blue btn">Login</button>
    </p>
</form>